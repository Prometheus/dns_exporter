package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"git.slxh.eu/prometheus/dns_exporter/dns"
)

func writeSOA(w io.Writer, zone, server string) {
	hdr := fmt.Sprintf(`{zone="%s",server="%s"}`, zone, server)
	serial, rtt, err := dns.GetSerialFromServer(zone, server)

	success := 0
	if err != nil {
		log.Printf("Error requesting serial of %s from %s: %s", zone, server, err)
	} else {
		success = 1
		fmt.Fprintf(w, "dns_soa%s %d\n", hdr, serial)
		if verbose {
			log.Printf("Result requesting serial of %s from %s: %v (%s)", zone, server, serial, rtt)
		}
	}

	fmt.Fprintf(w, "dns_success%s %v\n", hdr, success)
	fmt.Fprintf(w, "dns_duration_seconds%s %v\n", hdr, rtt.Seconds())
}

func soaHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	params := r.URL.Query()
	zones, ok := params["zone"]
	if !ok {
		zones, err = getZones()
		if err != nil {
			log.Print("Error retrieving zones")
			w.WriteHeader(500)
			return
		}
	}

	servers, ok := params["server"]
	if !ok {
		servers = make([]string, 0)
	}

	if checkLocalhost {
		hostname, err := os.Hostname()
		if err != nil {
			log.Print("Error retrieving hostname")
			w.WriteHeader(500)
			return
		}
		servers = append(servers, hostname)
	}

	for _, z := range zones {
		for _, s := range servers {
			writeSOA(w, z, s)
		}
	}
}
