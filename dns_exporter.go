package main

import (
	"flag"
	"log"
	"net/http"
	"os/exec"
	"strings"
)

var (
	zoneCommand string
	checkLocalhost bool
	verbose bool
)

func getZones() (zones []string, err error) {
	var out []byte

	out, err = exec.Command("bash", "-c", zoneCommand).Output()
	if err != nil {
		return
	}

	zones = strings.Split(strings.Trim(string(out), "\n"), "\n")
	return
}

func main() {
	var addr string

	flag.StringVar(&addr, "addr", ":9144", "Address and port to listen on")
	flag.StringVar(&zoneCommand, "zone-cmd", "", "Command to generate a zone list")
	flag.BoolVar(&checkLocalhost, "check-localhost", true, "Check the DNS on localhost as well")
	flag.BoolVar(&verbose, "verbose", false, "Log the results of queries")
	flag.Parse()

	log.Print("Listening on ", addr)
	http.HandleFunc("/metrics", dnssecHandler)
	http.HandleFunc("/soa/metrics", soaHandler)
	http.HandleFunc("/dnssec/metrics", dnssecHandler)
	log.Fatal(http.ListenAndServe(addr, nil))
}
