package dns

import (
	"errors"
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"github.com/miekg/dns"
)

// RootDS contains DS records for the root servers.
var RootDS = map[uint16]*dns.DS{
	20326: {Hdr: dns.RR_Header{Name: ".", Rrtype: 0x2b, Class: 0x1, Ttl: 0xd54, Rdlength: 0x0}, KeyTag: 0x4f66, Algorithm: 0x8, DigestType: 0x2, Digest: "e06d44b80b8f1d39a95c0b0d7c65d08458e880409bbc683457104237c7f8ec8d"},
	19036: {Hdr: dns.RR_Header{Name: ".", Rrtype: 0x2b, Class: 0x1, Ttl: 0xd54, Rdlength: 0x0}, KeyTag: 0x4a5c, Algorithm: 0x8, DigestType: 0x2, Digest: "49aac11d7b6f6446702e54a1607371607a1a41855200fd2ce1cdde32f24e8fb5"},
}

// Client is a DNS lookup client
type Client struct {
	udpClient dns.Client
	tcpClient dns.Client
	cache     map[string]map[uint16]*Result
	cacheLock sync.RWMutex
	cacheHits uint64
	cacheMiss uint64
}

func NewClient(dialer *net.Dialer) *Client {
	return &Client{
		cache:     make(map[string]map[uint16]*Result),
		udpClient: dns.Client{Dialer: dialer},
		tcpClient: dns.Client{Net: "tcp", Dialer: dialer},
	}
}

// Result contains the result of a DNS lookup
type Result struct {
	RRset []dns.RR
	RRsig []*dns.RRSIG
	TTL   time.Time
}

// DSs contains a key tag to DS record mapping
type DSs map[uint16]*dns.DS

// DNSKEYs contains a key tag to DNSKEY record mapping
type DNSKEYs map[uint16]*dns.DNSKEY

// Server returns a server connecting string for a nameserver
func server(ns string) string {
	return net.JoinHostPort(ns, DefaultPort)
}

// Resolve a dns record z of type t with a nameserver from set nss
func (c *Client) Resolve(z string, t uint16, nss []string) (*Result, time.Duration, error) {
	if r, ok := c.getCache(z, t); ok {
		return r, 0, nil
	}

	var in *dns.Msg
	var err error
	var rtt time.Duration

	r := new(Result)
	m := new(dns.Msg)
	m.SetEdns0(4096, true)
	m.SetQuestion(z, t)
	m.RecursionDesired = false

	// Try resolving against all nameservers
	shuffleStringSlice(nss)
	for _, n := range nss {
		var t time.Duration
		ns := server(n)
		in, t, err = c.Exchange(m, ns)
		rtt += t
		if err == nil {
			break
		}
	}

	if err != nil {
		return nil, rtt, fmt.Errorf("cannot resolve type %v for %q: %w", t, z, err)
	}

	// Return corresponding nameservers if answer is empty when requesting NS records
	rrs := in.Answer
	if t == dns.TypeNS && rrs == nil && len(in.Ns) > 0 && in.Ns[0].Header().Name == z {
		rrs = in.Ns
	}

	// Set TTL
	if len(rrs) > 0 {
		// Use the smallest returned TTL, with a maximum of 1 day
		ttl := uint32(86400)
		for _, r := range rrs {
			if r.Header().Ttl < ttl {
				ttl = r.Header().Ttl
			}
		}

		// Convert the TTL to a time.Time
		d := time.Second * time.Duration(ttl)
		r.TTL = time.Now().Add(d)
	}

	// Split RRset and RRsig
	for _, a := range rrs {
		if a.Header().Rrtype == dns.TypeRRSIG {
			r.RRsig = append(r.RRsig, a.(*dns.RRSIG))
		} else {
			r.RRset = append(r.RRset, a)
		}
	}

	// Cache result
	c.setCache(z, t, r)

	return r, rtt, nil
}

func (c *Client) Exchange(m *dns.Msg, address string) (r *dns.Msg, rtt time.Duration, err error) {
	r, rtt, err = c.udpClient.Exchange(m, address)
	if err != nil {
		return
	}

	if r.Truncated {
		return c.tcpClient.Exchange(m, address)
	}

	return
}

// Verify a Result using the given DNSKEYs
func (r *Result) Verify(keys DNSKEYs) error {
	for _, rrsig := range r.RRsig {
		// Check expiry
		if !rrsig.ValidityPeriod(time.Now()) {
			continue
		}

		// Check for a corresponding key
		key, ok := keys[rrsig.KeyTag]
		if !ok {
			continue
		}

		// Return if signature is valid
		if err := rrsig.Verify(key, r.RRset); err == nil {
			return nil
		}
	}

	return errors.New("no valid signature")
}

// GetDNSKEYs returns the DNSKEY records for a zone using the given nameservers.
// These DNSKEYs are verified using the DS records given.
func (c *Client) GetDNSKEYs(zone string, nss []string, dss DSs) (DNSKEYs, time.Duration, error) {
	r, rtt, err := c.Resolve(zone, dns.TypeDNSKEY, nss)

	if err != nil || len(r.RRset) == 0 {
		return nil, rtt, err
	}

	ksks := make(DNSKEYs)
	keys := make(DNSKEYs)

	// Index keys by tag
	for _, a := range r.RRset {
		k := a.(*dns.DNSKEY)
		t := k.KeyTag()

		// Add all key to keyset
		keys[t] = k

		// Add correct KSK to KSKs
		if ds, ok := dss[t]; ok && k.ToDS(ds.DigestType).Digest == ds.Digest {
			ksks[t] = k
		}
	}

	// Verify the keys
	if err := r.Verify(ksks); err != nil {
		return nil, rtt, err
	}

	return keys, rtt, nil
}

// GetDSs returns the DS records for a zone using the given nameservers.
// These DS records are verified using the given DNSKEY records.
func (c *Client) GetDSs(zone string, nss []string, keys DNSKEYs) (DSs, time.Duration) {
	r, rtt, err := c.Resolve(zone, dns.TypeDS, nss)
	if err != nil {
		return nil, rtt
	}

	ds := make(DSs)
	for _, r := range r.RRset {
		if r.Header().Rrtype == dns.TypeDS {
			d := r.(*dns.DS)
			ds[d.KeyTag] = d
		}
	}

	if err := r.Verify(keys); err != nil {
		return nil, rtt
	}

	return ds, rtt
}

// GetNSs returns the nameservers for a zone using the given nameservers.
// The NS records are not verified with DNSSEC.
func (c *Client) GetNSs(zone string, nss []string) ([]string, time.Duration, error) {
	r, rtt, err := c.Resolve(zone, dns.TypeNS, nss)
	if err != nil || r == nil {
		return nil, rtt, err
	}

	ns := make([]string, 0)
	for _, r := range r.RRset {
		if r.Header().Rrtype == dns.TypeNS {
			ns = append(ns, r.(*dns.NS).Ns)
		}
	}

	return ns, rtt, nil
}

// TraceSerial traces a serial for a zone from the root zone.
func (c *Client) TraceSerial(zone string) (serial uint32, expire uint32, sec uint8, rtt time.Duration, err error) {
	// Get root zone information
	var dnskeys DNSKEYs
	zones := getZones(zone)
	nss := RootServers
	dnskeys, rtt, err = c.GetDNSKEYs(".", nss, RootDS)

	// Loop trough zones to resolve
	var dss DSs
	var t time.Duration
	for _, z := range zones {
		if err != nil {
			return
		}

		if dnskeys == nil {
			break
		}
		oldNss := nss

		// Retrieve NS records for zone
		var newNss []string
		newNss, t, err = c.GetNSs(z, nss)
		if err != nil {
			return
		}

		if newNss == nil || len(newNss) == 0 {
			continue
		}
		nss = newNss
		rtt += t

		// Retrieve DS records for zone
		dss, t = c.GetDSs(z, oldNss, dnskeys)
		if dss == nil {
			continue
		}
		rtt += t

		// Retrieve DNSKEY records for zone
		dnskeys, t, err = c.GetDNSKEYs(z, nss, dss)
		rtt += t
	}

	// Retrieve the serial
	var r *Result
	r, t, err = c.Resolve(zone, dns.TypeSOA, nss)
	if err != nil {
		return
	}
	rtt += t

	// Check for a result
	if r == nil || len(r.RRset) == 0 {
		err = errors.New("failure resolving")
		return
	}

	// Check security
	if err := r.Verify(dnskeys); err == nil {
		sec = 1
	}

	// Get first RRSIG expiry
	if len(r.RRsig) > 0 {
		expire = r.RRsig[0].Expiration
	} else {
		expire = 0
	}

	// Get serial
	soa, ok := r.RRset[0].(*dns.SOA)
	if !ok {
		err = fmt.Errorf("not a SOA record: %v", r.RRset)
		return
	}
	serial = soa.Serial

	return
}

func (c *Client) setCache(z string, t uint16, r *Result) {
	c.cacheLock.Lock()
	defer c.cacheLock.Unlock()

	if _, ok := c.cache[z]; !ok {
		c.cache[z] = make(map[uint16]*Result)
	}

	c.cache[z][t] = r
}

func (c *Client) getCache(z string, t uint16) (*Result, bool) {
	c.cacheLock.RLock()
	defer c.cacheLock.RUnlock()

	if _, ok := c.cache[z]; !ok {
		atomic.AddUint64(&c.cacheMiss, 1)
		return nil, false
	}

	r, ok := c.cache[z][t]
	ok = ok && r.TTL.Sub(time.Now()) > 0
	if ok {
		atomic.AddUint64(&c.cacheHits, 1)
	} else {
		atomic.AddUint64(&c.cacheMiss, 1)
	}

	return r, ok
}

func (c *Client) CacheStats() (hits, misses uint64) {
	return c.cacheHits, c.cacheMiss
}
