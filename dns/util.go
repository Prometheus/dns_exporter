package dns

import (
	"math/rand"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func shuffleStringSlice(a []string) {
	rand.Shuffle(len(a), func(i, j int) {
        a[i], a[j] = a[j], a[i]
	})
}

func reverseStringSlice(a []string) []string {
	for i := len(a)/2 - 1; i >= 0; i-- {
		opp := len(a) - 1 - i
		a[i], a[opp] = a[opp], a[i]
	}

	return a
}

func getZones(zone string) []string {
	parts := strings.Split(zone, ".")
	for i := range parts {
		parts[i] = strings.Join(parts[i:], ".")
	}

	return reverseStringSlice(parts)[1:]
}
