package dns

import (
	"fmt"
	"github.com/miekg/dns"
	"net"
	"time"
)

const (
	// DefaultPort is the default DNS port
	DefaultPort = "53"
)

// RootServers contains the list of root DNS servers
var RootServers = []string{
	"a.root-servers.net.",
	"b.root-servers.net.",
	"c.root-servers.net.",
	"d.root-servers.net.",
	"e.root-servers.net.",
	"f.root-servers.net.",
	"g.root-servers.net.",
	"h.root-servers.net.",
	"i.root-servers.net.",
	"j.root-servers.net.",
	"k.root-servers.net.",
	"l.root-servers.net.",
	"m.root-servers.net.",
}

// Fqdn returns the FQDN for a given name.
func Fqdn(s string) string {
	return dns.Fqdn(s)
}

// GetSerialFromServer resolves a SOA record for a zone on a given server
// and returns the serial from the record.
func GetSerialFromServer(zone, server string) (uint32, time.Duration, error) {
	m := new(dns.Msg)
	m.SetQuestion(dns.Fqdn(zone), dns.TypeSOA)
	c := new(dns.Client)
	in, rtt, err := c.Exchange(m, net.JoinHostPort(server, DefaultPort))

	var serial uint32
	if err == nil && len(in.Answer) > 0 {
		soa, ok := in.Answer[0].(*dns.SOA)
		if !ok {
			err = fmt.Errorf("not a SOA record: %v", in.Answer)
			return serial, rtt, err
		}
		serial = soa.Serial
	}

	return serial, rtt, err
}
