package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"sync"
	"time"

	"git.slxh.eu/prometheus/dns_exporter/dns"
)

type syncWriter struct {
	w io.Writer
	sync.Mutex
}

func (w *syncWriter) Write(p []byte) (n int, err error) {
	w.Lock()
	defer w.Unlock()

	return w.w.Write(p)
}

func writeDNSSEC(w io.Writer, client *dns.Client, zone string) {
	start := time.Now()
	hdr := fmt.Sprintf(`{zone="%s"}`, zone)
	serial, expire, sec, rtt, err := client.TraceSerial(dns.Fqdn(zone))

	success := 0
	if err != nil {
		log.Printf("Error validating DNSSEC chain of %s: %s", zone, err)
	} else {
		success = 1
		fmt.Fprintf(w, "dnssec_soa%s %d\n", hdr, serial)
		fmt.Fprintf(w, "dnssec_rrsig_expiry%s %d\n", hdr, expire)
		if verbose {
			log.Printf("Result validating DNSSEC chain of %s: %v (secure: %v, expires at %s)", zone, serial, sec, time.Unix(int64(expire), 0))
		}
	}

	fmt.Fprintf(w, "dnssec_trace_duration_seconds%s %v\n", hdr, time.Since(start).Seconds())
	fmt.Fprintf(w, "dnssec_rtt_total_seconds%s %v\n", hdr, rtt.Seconds())
	fmt.Fprintf(w, "dnssec_secure%s %d\n", hdr, sec)
	fmt.Fprintf(w, "dnssec_success%s %v\n", hdr, success)
}

func dnssecHandler(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	zones, ok := params["zone"]

	if !ok {
		var err error
		zones, err = getZones()
		if err != nil {
			log.Print("Error retrieving zones")
			w.WriteHeader(500)
			return
		}
	}

	mw := &syncWriter{w: w}
	wg := sync.WaitGroup{}
	wg.Add(len(zones))

	c := dns.NewClient(&net.Dialer{Timeout: 250 * time.Millisecond})

	for _, z := range zones {
		zone := z
		go func() {
			writeDNSSEC(mw, c, zone)
			wg.Done()
		}()

		// Wait a bit to allow caching
		time.Sleep(50 * time.Millisecond)
	}

	wg.Wait()

	hits, misses := c.CacheStats()
	fmt.Fprintf(w, "dnssec_internal_cache_hits %d\n", hits)
	fmt.Fprintf(w, "dnssec_internal_cache_misses %v\n", misses)
}
