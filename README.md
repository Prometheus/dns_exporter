# Prometheus SOA exporter
Exports serials from SOA records.

Configure as follows:

```yaml
  - job_name: dns
    params:
      zone: ['one.example.com', 'two.example.com']
      server: ['ns1.example.com', 'ns2.example.com']
    static_configs:
      - targets: ['localhost:9144']
```
